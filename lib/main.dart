import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:omsa_medic/config/routes.dart';
import 'package:omsa_medic/router.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  // Transparent status bar
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
  ));

  // Lock portait
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  // Run app
  runApp(
    GetMaterialApp(
      theme: ThemeData(fontFamily: 'NunitoSans'),
      initialRoute: splashRoute,
      getPages: MainRouter().router(),
    ),
  );
}
