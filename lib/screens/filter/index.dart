import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/components/commons/subheading.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/interfaces/filter.dart';

class FilterScreen extends StatelessWidget {
  final IFilterArguments args = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: basePadding * 2,
                    left: basePadding,
                    right: basePadding,
                  ),
                  child: Row(
                    children: [
                      SubHeading('Filters'),
                      Spacer(),
                      Container(
                        child: ClickableContainer(
                          borderRadius: baseBorderRadius,
                          onTap: () => Get.back(),
                          color: Colors.transparent,
                          child: Icon(
                            Feather.x,
                            color: headingColor,
                            size: 30,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                Divider(),
                Padding(padding: EdgeInsets.only(top: 10)),
                args.content
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: basePadding * 0.5,
              left: basePadding,
              right: basePadding,
            ),
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              width: Get.width,
              height: 50,
              child: FlatButton(
                textColor: Colors.white,
                shape: RoundedRectangleBorder(borderRadius: halfBorderRadius),
                onPressed: args.submitFilter,
                color: primaryColor,
                child: Text('Filter'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
