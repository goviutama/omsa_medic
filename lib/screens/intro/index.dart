import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/indicator_slide.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/controllers/intro.dart';
import 'package:omsa_medic/screens/intro/components/intro_1.dart';
import 'package:omsa_medic/screens/intro/components/intro_2.dart';
import 'package:omsa_medic/screens/intro/components/intro_3.dart';

class IntroScreen extends StatelessWidget {
  final IntroController c = Get.put(IntroController());
  final List<Widget> introScreens = [
    Intro1(),
    Intro2(),
    Intro3(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            controller: c.pageViewController,
            children: introScreens,
            onPageChanged: (index) => c.setCurrentPage(index),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: basePadding),
            alignment: Alignment.bottomCenter,
            child: IndicatorSlide(
              introScreens.length,
              c.currentPage,
              MainAxisAlignment.center,
            ),
          ),
        ],
      ),
    );
  }
}
