import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/config/styles.dart';

class Intro1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset('assets/images/draw/doctor_man.svg'),
        Padding(padding: EdgeInsets.only(top: basePadding * 1.5)),
        Heading('Search Doctors'),
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: basePadding,
          ),
          child: Text(
            'Get the list of the best doctors you need',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: captionColor,
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }
}
