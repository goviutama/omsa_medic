import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/config/styles.dart';

class Intro2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset('assets/images/draw/clinic.svg'),
        Padding(padding: EdgeInsets.only(top: basePadding * 1.5)),
        Heading('Search Medical Facility'),
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: basePadding,
          ),
          child: Text(
            'Find the health facilities you need near you',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: captionColor,
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }
}
