import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/routes.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:flutter_svg/svg.dart';
import 'package:omsa_medic/components/commons/heading.dart';

class Intro3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/images/draw/omsa_medic_card.svg'),
            Padding(padding: EdgeInsets.only(top: basePadding * 1.5)),
            Heading('Member Card'),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: basePadding,
              ),
              child: Text(
                'Find the health facilities you need near you',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: captionColor,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
        Container(
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          margin: EdgeInsets.only(bottom: 100),
          child: FlatButton(
            height: 50,
            minWidth: double.infinity,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            color: primaryColor,
            child: Text(
              'Sign In',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Get.toNamed(authLoginRoute);
            },
          ),
        ),
      ],
    );
  }
}
