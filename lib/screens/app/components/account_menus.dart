import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:omsa_medic/config/styles.dart';

class AccountMenus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: basePadding,
            vertical: 10,
          ),
          child: Text(
            'ACCOUNT',
            style: TextStyle(
              color: captionColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: basePadding),
          title: Text('Change Password'),
          subtitle: Text('Change your password here'),
          trailing: Icon(
            Feather.chevron_right,
            size: 16,
          ),
          onTap: () {
            print('change password');
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: Divider(),
        ),
        Padding(padding: EdgeInsets.only(top: 20)),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: basePadding,
            vertical: 10,
          ),
          child: Text(
            'GENERAL',
            style: TextStyle(
              color: captionColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: basePadding),
          title: Text('Term & Conditions'),
          trailing: Icon(
            Feather.chevron_right,
            size: 16,
          ),
          onTap: () {
            print('tapped');
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: Divider(),
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: basePadding),
          title: Text('Privacy Policy'),
          trailing: Icon(
            Feather.chevron_right,
            size: 16,
          ),
          onTap: () {
            print('tapped');
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: Divider(),
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: basePadding),
          title: Text('Contact'),
          trailing: Icon(
            Feather.chevron_right,
            size: 16,
          ),
          onTap: () {
            print('tapped');
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: Divider(),
        ),
      ],
    );
  }
}
