import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:omsa_medic/components/commons/subheading.dart';
import 'package:omsa_medic/config/styles.dart';

class AccountHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            image: DecorationImage(
              image: AssetImage('assets/images/profile.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(left: 20)),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SubHeading('Govi Utama'),
              Padding(padding: EdgeInsets.only(top: 5)),
              Text(
                '0010/JKL/W/GOLD/F/B',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  color: primaryColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 10)),
              SizedBox(
                width: 95,
                child: InkWell(
                  onTap: () {
                    print('edit profile');
                  },
                  child: Row(
                    children: [
                      Text(
                        'Edit Profile',
                        style: TextStyle(color: captionColor),
                      ),
                      Icon(
                        Feather.chevron_right,
                        size: 20,
                        color: captionColor,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
