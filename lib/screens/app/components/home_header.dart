import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/components/commons/label.dart';
import 'package:omsa_medic/config/routes.dart';
import 'package:omsa_medic/config/styles.dart';

class HomeHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'assets/images/pattern/top-primary.png',
          width: Get.width,
        ),
        Container(
          margin: EdgeInsets.only(
            left: basePadding,
            top: basePadding * 2,
          ),
          alignment: Alignment.topLeft,
          width: 55,
          height: 55,
          decoration: BoxDecoration(
            borderRadius: baseBorderRadius,
            image: DecorationImage(
              image: AssetImage('assets/images/profile.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            top: basePadding * 2,
            right: basePadding,
          ),
          alignment: Alignment.topRight,
          child: ClickableContainer(
            color: Colors.transparent,
            borderRadius: baseBorderRadius,
            onTap: () => Get.toNamed(qrCodeRoute),
            child: Padding(
              padding: EdgeInsets.all(5),
              child: SvgPicture.asset(
                'assets/icons/qr-code.svg',
                width: 40,
                height: 40,
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: basePadding * 4.5),
          padding: EdgeInsets.all(basePadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Hello,',
                style: TextStyle(
                  color: headingColor,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: Get.width * 0.7,
                    child: Heading('Govi Utama'),
                  ),
                  Label('Aktif', secondaryColor),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 10)),
              Text(
                '0010/JKL/W/GOLD/F/B',
                style: TextStyle(
                  color: primaryColor,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
