import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/components/commons/subheading.dart';
import 'package:omsa_medic/config/routes.dart';
import 'package:omsa_medic/config/styles.dart';

class Menu {
  String text;
  String route;
  String icon;
  Menu(this.text, this.route, this.icon);
}

class HomeMenus extends StatelessWidget {
  final List<Menu> menus = [
    Menu('Member', memberRoute, 'assets/icons/member.svg'),
    Menu('Clinic', clinicRoute, 'assets/icons/clinic.svg'),
    Menu('Schedule', scheduleRoute, 'assets/icons/schedule.svg'),
    Menu('Appointment', appointmentRoute, 'assets/icons/appointment.svg'),
    Menu('Complaint', complaintRoute, 'assets/icons/complaint.svg'),
    Menu('History', historyRoute, 'assets/icons/history.svg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: Get.width,
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: SubHeading('What do you need?'),
        ),
        Padding(padding: EdgeInsets.only(top: 40)),
        Wrap(
          spacing: basePadding,
          runSpacing: basePadding,
          children: List<Widget>.generate(this.menus.length, (int index) {
            return Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                borderRadius: baseBorderRadius,
                boxShadow: [baseBoxShadow],
              ),
              child: ClickableContainer(
                color: Colors.white,
                borderRadius: baseBorderRadius,
                onTap: () => Get.toNamed(this.menus[index].route),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      this.menus[index].icon,
                      width: 50,
                      height: 50,
                    ),
                    Padding(padding: EdgeInsets.only(top: 7)),
                    Text(
                      this.menus[index].text,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ],
    );
  }
}
