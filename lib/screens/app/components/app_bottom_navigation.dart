import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/controllers/app.dart';

class BottomNavigationMenu {
  String icon;
  String iconActive;
  BottomNavigationMenu(this.icon, this.iconActive);
}

class AppBottomNavigation extends StatelessWidget {
  final AppController c = Get.put(AppController());
  final List menus = [
    BottomNavigationMenu(
      'assets/icons/home.svg',
      'assets/icons/home-active.svg',
    ),
    BottomNavigationMenu(
      'assets/icons/activity.svg',
      'assets/icons/activity-active.svg',
    ),
    BottomNavigationMenu(
      'assets/icons/account.svg',
      'assets/icons/account-active.svg',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: basePadding * 0.5,
        right: basePadding * 0.5,
        bottom: basePadding * 0.5,
      ),
      height: 65,
      width: Get.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(basePadding),
        boxShadow: [
          baseBoxShadow,
        ],
      ),
      child: Row(
        children: List<Widget>.generate(menus.length, (int index) {
          return Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    c.setCurrentNav(index);
                  },
                  child: Obx(() {
                    return Container(
                      padding: EdgeInsets.all(8),
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: (c.currentNav.value == index)
                            ? primaryLightenColor
                            : Colors.white,
                      ),
                      child: SvgPicture.asset((c.currentNav.value == index)
                          ? menus[index].iconActive
                          : menus[index].icon),
                    );
                  }),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
