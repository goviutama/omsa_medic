import 'package:flutter/material.dart';

class AppBottomPadding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 120),
    );
  }
}
