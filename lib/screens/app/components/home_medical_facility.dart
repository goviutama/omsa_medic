import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/subheading.dart';
import 'package:omsa_medic/components/items/item_medical_facility.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/interfaces/medical_facility.dart';

class HomeMedicalFacility extends StatelessWidget {
  final List<IMedicalFacility> items = [
    IMedicalFacility(
      image: 'assets/images/clinic-1.png',
      name: 'Bali Royal Hospital (BROH)',
      location: 'Renon Denpasar - 3.2km',
      telp: '+6227178282817',
    ),
    IMedicalFacility(
      image: 'assets/images/clinic-2.png',
      name: 'Bali Medical',
      location: 'Mahendradata - 3.2km',
      telp: '+6227178282817',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: Get.width,
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: SubHeading('Medical facility nearby you'),
        ),
        Container(
          padding: EdgeInsets.only(top: 20),
          height: 170,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: List<Widget>.generate(this.items.length, (int index) {
              return ItemMedicalFacility(
                item: this.items[index],
                index: index,
                isLastIndex: (index == this.items.length - 1),
                width: 320,
              );
            }),
          ),
        )
      ],
    );
  }
}
