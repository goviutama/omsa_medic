import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/indicator_slide.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/controllers/banner.dart';

class HomeBanner extends StatelessWidget {
  final BannerController c = Get.put(BannerController());
  final List<String> banners = [
    'assets/images/banner/1.png',
    'assets/images/banner/1.png',
    'assets/images/banner/1.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CarouselSlider(
          items: List<Widget>.generate(banners.length, (int index) {
            return Container(
              width: Get.width,
              margin: EdgeInsets.symmetric(horizontal: basePadding),
              decoration: BoxDecoration(
                color: Colors.amber,
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                  image: AssetImage(banners[index]),
                  fit: BoxFit.fill,
                ),
              ),
            );
          }),
          options: CarouselOptions(
            aspectRatio: 16 / 6,
            viewportFraction: 1,
            enableInfiniteScroll: true,
            onPageChanged: (int index, CarouselPageChangedReason reason) {
              c.setCurrentPage(index);
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: IndicatorSlide(
            banners.length,
            c.currentPage,
            MainAxisAlignment.start,
          ),
        ),
      ],
    );
  }
}
