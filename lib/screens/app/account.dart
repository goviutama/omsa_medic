import 'package:flutter/material.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/screens/app/components/account_header.dart';
import 'package:omsa_medic/screens/app/components/account_menus.dart';

class AccountScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(
                top: basePadding * 2,
                left: basePadding,
                right: basePadding,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Heading('Account'),
                  Padding(padding: EdgeInsets.only(top: basePadding)),
                  AccountHeader(),
                  Padding(padding: EdgeInsets.only(top: basePadding)),
                ],
              ),
            ),
            AccountMenus(),
            Padding(padding: EdgeInsets.only(top: basePadding)),
            Container(
              padding: EdgeInsets.symmetric(horizontal: basePadding),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                highlightColor: Colors.transparent,
                splashColor: primaryLightenColor,
                height: 50,
                minWidth: double.infinity,
                onPressed: () {
                  print('logout');
                },
                child: Text(
                  'Log Out',
                  style: TextStyle(
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
