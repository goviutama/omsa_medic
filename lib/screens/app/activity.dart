import 'package:flutter/material.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/config/styles.dart';

class ActivityScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: basePadding * 2,
            horizontal: basePadding,
          ),
          child: Column(
            children: [Heading('Activity')],
          ),
        ),
      ),
    );
  }
}
