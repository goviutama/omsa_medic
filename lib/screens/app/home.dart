import 'package:flutter/material.dart';
import 'package:omsa_medic/screens/app/components/home_banner.dart';
import 'package:omsa_medic/screens/app/components/home_header.dart';
import 'package:omsa_medic/screens/app/components/home_medical_facility.dart';
import 'package:omsa_medic/screens/app/components/home_menus.dart';
import 'package:omsa_medic/screens/app/components/app_bottom_padding.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            HomeHeader(),
            Padding(padding: EdgeInsets.only(top: 20)),
            HomeBanner(),
            Padding(padding: EdgeInsets.only(top: 20)),
            HomeMenus(),
            Padding(padding: EdgeInsets.only(top: 40)),
            HomeMedicalFacility(),
            AppBottomPadding(),
          ],
        ),
      ),
    );
  }
}
