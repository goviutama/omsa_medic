import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/controllers/app.dart';
import 'package:omsa_medic/screens/app/account.dart';
import 'package:omsa_medic/screens/app/activity.dart';
import 'package:omsa_medic/screens/app/components/app_bottom_navigation.dart';
import 'package:omsa_medic/screens/app/home.dart';

class AppScreen extends StatelessWidget {
  final AppController c = Get.put(AppController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Obx(() {
            return IndexedStack(
              index: c.currentNav.value,
              children: [
                HomeScreen(),
                ActivityScreen(),
                AccountScreen(),
              ],
            );
          }),
          Container(
            alignment: Alignment.bottomCenter,
            child: AppBottomNavigation(),
          )
        ],
      ),
    );
  }
}
