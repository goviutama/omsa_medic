import 'package:flutter/material.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/components/commons/top_bar.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/screens/member/components/member_type.dart';

class MemberScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          TopBar(
            title: '',
            subtitle: '',
          ),
          Padding(
            padding: EdgeInsets.only(
              left: basePadding,
              right: basePadding,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Heading('Govi Utama'),
                Padding(padding: EdgeInsets.only(top: 5)),
                Text(
                  '2830928302180312',
                  style: TextStyle(
                    color: primaryColor,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 5)),
                Text(
                  'PT. Solusi Anak Sakti',
                  style: TextStyle(
                    color: greyColor,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                MemberType(),
                Padding(padding: EdgeInsets.only(top: 20)),
                Text(
                  'Benefits',
                  style: TextStyle(
                    color: headingColor,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                  style: TextStyle(
                    color: captionColor,
                    fontSize: 16,
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                InkWell(
                  onTap: () {},
                  child: Text(
                    'Read More',
                    style: TextStyle(
                      color: primaryColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 40)),
                Container(
                  alignment: Alignment.center,
                  child: ClickableContainer(
                    color: Colors.white,
                    onTap: () {},
                    child: Container(
                      width: 360,
                      height: 240,
                      decoration: BoxDecoration(
                        borderRadius: baseBorderRadius,
                        image: DecorationImage(
                          image: AssetImage(
                            'assets/images/draw/omsa_medic_card_empty.png',
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
