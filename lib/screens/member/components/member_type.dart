import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/styles.dart';

class MemberType extends StatelessWidget {
  final String type;
  MemberType({this.type});

  @override
  Widget build(BuildContext context) {
    switch (this.type) {
      case 'gold':
        return Container();
      default:
        return Container(
          width: Get.width,
          height: 60,
          decoration: BoxDecoration(
            color: secondaryColor,
            borderRadius: baseBorderRadius,
          ),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: SvgPicture.asset('assets/icons/member_type.svg'),
              ),
              Text(
                'Gold (A) - Lokal',
                style: TextStyle(
                  color: headingColor,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        );
    }
  }
}
