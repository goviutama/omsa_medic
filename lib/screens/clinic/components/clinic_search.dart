import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:omsa_medic/config/styles.dart';

class ClinicSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: basePadding),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          boxShadow: [baseBoxShadow],
        ),
        child: TextFormField(
          textInputAction: TextInputAction.search,
          decoration: InputDecoration(
            fillColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(vertical: 15),
            prefixIcon: Icon(
              Feather.search,
              color: primaryColor,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(50),
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
            ),
            hintText: 'Cari...',
            filled: true,
          ),
        ),
      ),
    );
  }
}
