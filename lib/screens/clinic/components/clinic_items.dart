import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/items/item_medical_facility.dart';
import 'package:omsa_medic/interfaces/medical_facility.dart';

class ClinicItems extends StatelessWidget {
  final List<IMedicalFacility> items = [
    IMedicalFacility(
      image: 'assets/images/clinic-1.png',
      name: 'Bali Royal Hospital (BROH)',
      location: 'Renon Denpasar - 3.2km',
      telp: '+6227178282817',
    ),
    IMedicalFacility(
      image: 'assets/images/clinic-2.png',
      name: 'Bali Medical',
      location: 'Mahendradata - 3.2km',
      telp: '+6227178282817',
    ),
    IMedicalFacility(
      image: 'assets/images/clinic-1.png',
      name: 'Bali Royal Hospital (BROH)',
      location: 'Renon Denpasar - 3.2km',
      telp: '+6227178282817',
    ),
    IMedicalFacility(
      image: 'assets/images/clinic-2.png',
      name: 'Bali Medical',
      location: 'Mahendradata - 3.2km',
      telp: '+6227178282817',
    ),
    IMedicalFacility(
      image: 'assets/images/clinic-1.png',
      name: 'Bali Royal Hospital (BROH)',
      location: 'Renon Denpasar - 3.2km',
      telp: '+6227178282817',
    ),
    IMedicalFacility(
      image: 'assets/images/clinic-2.png',
      name: 'Bali Medical',
      location: 'Mahendradata - 3.2km',
      telp: '+6227178282817',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List<Widget>.generate(this.items.length, (int index) {
        return ItemMedicalFacility(
          item: this.items[index],
          index: index,
          isLastIndex: true,
          width: Get.width,
          height: 130,
        );
      }),
    );
  }
}
