import 'package:flutter/material.dart';
import 'package:omsa_medic/config/styles.dart';

class ClinicFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: Text(
            'Fasilitas Kesehatan',
            style: TextStyle(
              color: headingColor,
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 10)),
        Divider(),
        Padding(padding: EdgeInsets.only(top: 10)),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: basePadding),
          child: Text(
            'Jenis Layanan',
            style: TextStyle(
              color: headingColor,
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ],
    );
  }
}
