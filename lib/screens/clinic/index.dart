import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/top_bar.dart';
import 'package:omsa_medic/config/routes.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/interfaces/filter.dart';
import 'package:omsa_medic/screens/clinic/components/clinic_filter.dart';
import 'package:omsa_medic/screens/clinic/components/clinic_items.dart';
import 'package:omsa_medic/screens/clinic/components/clinic_search.dart';

class ClinicScreen extends StatelessWidget {
  final IFilterArguments filterArguments = IFilterArguments(
    content: ClinicFilter(),
    submitFilter: () {
      Get.back();
      Get.offNamed(clinicRoute);
      print('test');
    },
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TopBar(
                    title: 'Fasilitas Kesehatan',
                    subtitle: 'Clinic',
                  ),
                  ClinicSearch(),
                  Padding(padding: EdgeInsets.only(top: 20)),
                  ClinicItems(),
                  Padding(padding: EdgeInsets.only(bottom: basePadding * 2.5)),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: basePadding * 0.5),
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: 140,
                height: 50,
                child: FlatButton(
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  onPressed: () => Get.toNamed(
                    filterRoute,
                    arguments: filterArguments,
                  ),
                  color: primaryColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Feather.filter),
                      Padding(padding: EdgeInsets.only(left: 10)),
                      Text('Filter'),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
