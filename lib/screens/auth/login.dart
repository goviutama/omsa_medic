import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/screens/auth/components/form_login.dart';
import 'package:omsa_medic/screens/auth/components/header_login.dart';

class AuthLoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryDarkenColor,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: SizedBox(
            height: Get.height,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: HeaderLogin(),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    vertical: basePadding * 1.3,
                    horizontal: basePadding,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    ),
                  ),
                  child: FormLogin(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
