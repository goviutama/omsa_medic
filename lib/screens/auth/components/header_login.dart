import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/styles.dart';

class HeaderLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SvgPicture.asset(
          'assets/images/pattern/top.svg',
          width: Get.width,
        ),
        Container(
          padding: EdgeInsets.only(right: basePadding * 0.5),
          alignment: Alignment.bottomRight,
          child: SvgPicture.asset(
            'assets/images/draw/doctor_woman.svg',
            width: Get.width * 0.5,
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 100, left: basePadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Welcome',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 35,
                  fontWeight: FontWeight.w800,
                ),
              ),
              Text(
                'Back !',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 35,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
