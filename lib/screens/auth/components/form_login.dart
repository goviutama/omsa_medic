import 'dart:async';

import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:omsa_medic/components/commons/heading.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/controllers/auth.dart';

class FormLogin extends StatelessWidget {
  final AuthController c = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: c.formLogin,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Heading('Sign In'),
            Padding(padding: EdgeInsets.only(top: 10)),
            TextFormField(
              controller: c.memberCodeInput,
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: 'Kode Member'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Kode member harus diisi!';
                }
                return null;
              },
            ),
            Padding(padding: EdgeInsets.only(top: 20)),
            TextFormField(
              obscureText: true,
              textInputAction: TextInputAction.send,
              onFieldSubmitted: (value) {
                Timer(Duration(milliseconds: 500), () => c.submitLogin());
              },
              decoration: InputDecoration(labelText: 'Password'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Password harus diisi!';
                }
                return null;
              },
            ),
            Padding(padding: EdgeInsets.only(top: 30)),
            InkWell(
              onTap: () {
                print('forgot password');
              },
              child: Text(
                'Forgot password?',
                style: TextStyle(
                  color: primaryColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 30)),
            FlatButton(
              height: 50,
              minWidth: double.infinity,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              color: primaryColor,
              child: Text(
                'Sign In',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => c.submitLogin(),
            ),
          ],
        ),
      ),
    );
  }
}
