import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/controllers/auth.dart';

class SplashScreen extends StatelessWidget {
  final AuthController c = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    c.checkAuth();

    return Scaffold(
      body: Container(
        height: Get.height,
        width: Get.width,
        color: primaryDarkenColor,
        child: Stack(
          children: [
            Container(
              alignment: Alignment.center,
              child: Image.asset(
                'assets/images/logo/splash.png',
                width: Get.width * 0.7,
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              child: SvgPicture.asset(
                'assets/images/pattern/bottom.svg',
                width: Get.width,
              ),
            )
          ],
        ),
      ),
    );
  }
}
