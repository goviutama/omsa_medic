import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/components/commons/subheading.dart';
import 'package:omsa_medic/config/styles.dart';
import 'package:omsa_medic/controllers/qr_code.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCodeScreen extends StatelessWidget {
  final QrCodeController c = Get.put(QrCodeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryDarkenColor,
      body: Container(
        height: Get.height,
        width: Get.width,
        color: primaryDarkenColor,
        child: Stack(
          children: [
            Container(
              alignment: Alignment.topCenter,
              padding: EdgeInsets.only(
                top: basePadding * 2,
                left: basePadding,
                right: basePadding,
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: ClickableContainer(
                      borderRadius: baseBorderRadius,
                      onTap: () => Get.back(),
                      color: Colors.transparent,
                      child: Icon(
                        Feather.x,
                        color: Colors.white,
                        size: 36,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 40)),
                  Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: baseBorderRadius,
                      image: DecorationImage(
                        image: AssetImage('assets/images/profile.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 20)),
                  Text(
                    'Govi Utama',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 5)),
                  Text(
                    '0010/JKL/W/GOLD/F/B',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 40)),
                  Container(
                    padding: EdgeInsets.all(basePadding),
                    width: Get.width * 0.8,
                    height: 400,
                    decoration: BoxDecoration(
                      borderRadius: baseBorderRadius,
                      color: Colors.white,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SubHeading('Qr Code'),
                        Padding(padding: EdgeInsets.only(top: 20)),
                        QrImage(
                          data: 'Govi Utama',
                          version: QrVersions.auto,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              child: SvgPicture.asset(
                'assets/images/pattern/bottom.svg',
                width: Get.width,
              ),
            )
          ],
        ),
      ),
    );
  }
}
