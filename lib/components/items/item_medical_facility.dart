import 'package:flutter/material.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/config/styles.dart';

class ItemMedicalFacility extends StatelessWidget {
  final int index;
  final dynamic item;
  final bool isLastIndex;
  final double width;
  final double height;
  ItemMedicalFacility({
    this.item,
    this.index,
    this.isLastIndex,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: basePadding,
        right: (isLastIndex) ? basePadding : 0,
        top: 10,
        bottom: 10,
      ),
      width: this.width,
      height: this.height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [baseBoxShadow],
      ),
      child: ClickableContainer(
        color: Colors.white,
        borderRadius: baseBorderRadius,
        onTap: () {
          print('test');
        },
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: DecorationImage(
                  image: AssetImage(this.item.image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      this.item.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 5)),
                    Text(
                      this.item.location,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 5)),
                    Text(
                      this.item.telp,
                      style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
