import 'package:flutter/material.dart';

class ClickableContainer extends StatelessWidget {
  final BorderRadius borderRadius;
  final VoidCallback onTap;
  final Widget child;
  final Color color;

  ClickableContainer({
    this.borderRadius,
    this.onTap,
    this.child,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: this.borderRadius,
      color: this.color,
      child: InkWell(
        borderRadius: this.borderRadius,
        onTap: this.onTap,
        child: this.child,
      ),
    );
  }
}
