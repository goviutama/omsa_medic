import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:omsa_medic/config/styles.dart';

class IndicatorSlide extends StatelessWidget {
  final int length;
  final RxInt currentPage;
  final MainAxisAlignment position;

  IndicatorSlide(this.length, this.currentPage, this.position);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Row(
        mainAxisAlignment: position,
        children: List<Widget>.generate(length, (int index) {
          return AnimatedContainer(
            duration: Duration(milliseconds: 300),
            height: 10,
            width: (currentPage.value == index) ? 30 : 10,
            margin: EdgeInsets.symmetric(
              vertical: basePadding * 0.7,
              horizontal: 5,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: (currentPage.value == index)
                  ? primaryColor
                  : primaryLightenColor,
            ),
          );
        }),
      );
    });
  }
}
