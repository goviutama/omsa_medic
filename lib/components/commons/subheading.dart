import 'package:flutter/material.dart';
import 'package:omsa_medic/config/styles.dart';

class SubHeading extends StatelessWidget {
  final String text;
  SubHeading(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: headingColor,
        fontSize: 24,
        fontWeight: FontWeight.w800,
      ),
    );
  }
}
