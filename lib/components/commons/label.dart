import 'package:flutter/material.dart';
import 'package:omsa_medic/config/styles.dart';

class Label extends StatelessWidget {
  final String text;
  final Color color;
  Label(this.text, this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: basePadding * 0.5,
        vertical: basePadding * 0.25,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        color: this.color,
      ),
      child: Text(this.text),
    );
  }
}
