import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/components/commons/clickable_container.dart';
import 'package:omsa_medic/config/styles.dart';

class TopBar extends StatelessWidget {
  final String title;
  final String subtitle;
  TopBar({
    this.title,
    this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: basePadding * 2,
        left: basePadding,
        right: basePadding,
        bottom: basePadding,
      ),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(),
            child: ClickableContainer(
              borderRadius: halfBorderRadius,
              onTap: () => Get.back(),
              color: shadowColor,
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Icon(
                  Feather.chevron_left,
                  color: headingColor,
                  size: 30,
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  this.subtitle,
                  style: TextStyle(
                    color: captionColor,
                  ),
                ),
                Text(
                  this.title,
                  style: TextStyle(
                    color: headingColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
