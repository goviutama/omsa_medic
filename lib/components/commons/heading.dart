import 'package:flutter/material.dart';
import 'package:omsa_medic/config/styles.dart';

class Heading extends StatelessWidget {
  final String text;
  Heading(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: headingColor,
        fontSize: 27,
        fontWeight: FontWeight.w900,
      ),
    );
  }
}
