import 'package:get/get.dart';
import 'package:flutter/material.dart';

class IntroController extends GetxController {
  var currentPage = 0.obs;
  final pageViewController = PageController(initialPage: 0);

  setCurrentPage(index) {
    currentPage.value = index;
    update();
  }
}
