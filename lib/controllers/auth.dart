import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omsa_medic/config/routes.dart';

class AuthController extends GetxController {
  final formLogin = GlobalKey<FormState>();
  final memberCodeInput = TextEditingController();
  final passwordInput = TextEditingController();

  submitLogin() {
    // if (formLogin.currentState.validate()) {
    Get.offNamed(appRoute);
    // }
  }

  checkAuth() {
    return Timer(Duration(seconds: 2), () {
      Get.offNamed(introRoute);
    });
  }
}
