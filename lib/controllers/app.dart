import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class AppController extends GetxController {
  var currentNav = 0.obs;

  setCurrentNav(index) {
    currentNav.value = index;
    update();
  }
}
