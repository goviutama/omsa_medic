import 'package:get/get.dart';

class BannerController extends GetxController {
  var currentPage = 0.obs;

  setCurrentPage(index) {
    currentPage.value = index;
    update();
  }
}
