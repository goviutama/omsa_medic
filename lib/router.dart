import 'package:get/get.dart';
import 'package:omsa_medic/config/routes.dart';
import 'package:omsa_medic/screens/app/index.dart';
import 'package:omsa_medic/screens/auth/login.dart';
import 'package:omsa_medic/screens/clinic/index.dart';
import 'package:omsa_medic/screens/filter/index.dart';
import 'package:omsa_medic/screens/intro/index.dart';
import 'package:omsa_medic/screens/member/index.dart';
import 'package:omsa_medic/screens/qr-code/index.dart';
import 'package:omsa_medic/screens/splash/index.dart';

class MainRouter {
  router() {
    return [
      GetPage(name: appRoute, page: () => AppScreen()),
      GetPage(name: authLoginRoute, page: () => AuthLoginScreen()),
      GetPage(name: introRoute, page: () => IntroScreen()),
      GetPage(name: splashRoute, page: () => SplashScreen()),
      GetPage(name: qrCodeRoute, page: () => QrCodeScreen()),
      GetPage(name: memberRoute, page: () => MemberScreen()),
      GetPage(name: clinicRoute, page: () => ClinicScreen()),
      GetPage(name: filterRoute, page: () => FilterScreen()),
    ];
  }
}
