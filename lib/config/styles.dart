import 'package:flutter/painting.dart';

Color primaryColor = Color(0xffdd3342);
Color primaryDarkenColor = Color(0xffa7263a);
Color primaryLightenColor = Color(0xfff8d6d9);
Color secondaryColor = Color(0xffffc000);
Color headingColor = Color(0xff333333);
Color captionColor = Color(0xff6d6a6b);
Color greyColor = Color(0xffa7a7a7);
Color shadowColor = Color(0xffebebeb);

double basePadding = 28;

BoxShadow baseBoxShadow = BoxShadow(
  color: shadowColor,
  blurRadius: 10,
  spreadRadius: -1,
  offset: Offset(0, 3),
);

BorderRadius baseBorderRadius = BorderRadius.circular(15);
BorderRadius halfBorderRadius = BorderRadius.circular(7.5);
