import 'package:flutter/cupertino.dart';

class IFilterArguments {
  Widget content;
  Function submitFilter;
  IFilterArguments({this.content, this.submitFilter});
}
